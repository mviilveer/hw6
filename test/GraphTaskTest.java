import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   private GraphTask.Graph createGraph()
   {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex first = g.createVertex("1");
      GraphTask.Vertex second = g.createVertex("2");
      GraphTask.Vertex third = g.createVertex("3");
      g.createArc("arc32", third, second);
      g.createArc("arc31", third, first);
      g.createArc("arc21", second, first);
      return g;
   }

   private GraphTask.Graph createExtendedGraph()
   {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("G");
      GraphTask.Vertex first = g.createVertex("1");
      GraphTask.Vertex second = g.createVertex("2");
      GraphTask.Vertex third = g.createVertex("3");
      GraphTask.Vertex fourth = g.createVertex("4");
      g.createArc("arc32", third, second);
      g.createArc("arc31", third, first);
      g.createArc("arc21", second, first);
      g.createArc("arc34", third, fourth);
      g.createArc("arc41", fourth, first);
      return g;
   }

   @Test (timeout=20000)
   public void testSinglePathWithOneArc() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = createGraph();

      ArrayList<ArrayList<GraphTask.Arc>> paths = g.findLongestPaths(task.new Vertex("2"));
      for (ArrayList<GraphTask.Arc> path : paths) {
         assertEquals(path.size(), 1);
         assertEquals(path.get(path.size() - 1).target.id, "1");
      }
   }

   @Test (timeout=20000)
   public void testSinglePathWithManyArcs() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = createGraph();

      ArrayList<ArrayList<GraphTask.Arc>> paths = g.findLongestPaths(task.new Vertex("3"));
      for (ArrayList<GraphTask.Arc> path : paths) {
         assertEquals(path.size(), 2);
         assertEquals(path.get(path.size() - 1).target.id, "1");
      }
   }

   @Test (timeout=20000)
   public void testManyPaths() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = createExtendedGraph();

      ArrayList<ArrayList<GraphTask.Arc>> paths = g.findLongestPaths(task.new Vertex("3"));
      assertEquals(paths.size(), 2);
      ArrayList<GraphTask.Arc> arcsInPath1 = paths.get(0);
      assertEquals(arcsInPath1.size(), 2);
      ArrayList<GraphTask.Arc> arcsInPath2 = paths.get(1);
      assertEquals(arcsInPath2.size(), 2);
      for (ArrayList<GraphTask.Arc> path : paths) {
         assertEquals(path.get(path.size() - 1).target.id, "1");
      }
      assertNotEquals(paths.get(0), paths.get(1));
   }

   @Test (expected=RuntimeException.class)
   public void testVertexWithoutArcs() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = createExtendedGraph();
      g.createVertex("5");

      ArrayList<ArrayList<GraphTask.Arc>> paths = g.findLongestPaths(task.new Vertex("5"));
   }

   @Test (expected=RuntimeException.class)
   public void testVertexWithOnlyIncomingArcs() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = createExtendedGraph();

      ArrayList<ArrayList<GraphTask.Arc>> paths = g.findLongestPaths(task.new Vertex("1"));
   }

}

