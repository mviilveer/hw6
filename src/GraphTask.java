import java.util.*;

/**
 * This graph task contains solution for "longest path problem" in simple graph
 * There is a method getLongestPath() in Graph class to resolve that problem
 */
public class GraphTask {

    /**
     * Main method
     * @param args String[]
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    private void run() {
        Graph suur = new Graph("G");
        suur.createRandomSimpleGraph(2500, 2520);
        System.out.println(suur.getMaxPathLength());
        System.out.println(suur.findLongestPaths(suur.first));


        // loome uue graafi
        Graph g = new Graph("G");

        // lisame graafi tipud
        Vertex first = g.createVertex("1");
        Vertex second = g.createVertex("2");
        Vertex third = g.createVertex("3");
        // seome tipud kokku
        g.createArc("arc32", third, second);
        g.createArc("arc31", third, first);
        g.createArc("arc21", second, first);

        // leiame pikimad teekonnad
        ArrayList<ArrayList<Arc>> paths = g.findLongestPaths(third);
        for (ArrayList<Arc> path : paths) {
            System.out.println(path);
            // prindime välja kõige kaugemal asuvad tipud
            System.out.println(path.get(path.size() - 1).target);
        }

//      System.out.println(g.findLongestPaths(first));
        System.out.println(g.findLongestPaths(second));
        System.out.println(g.findLongestPaths(third));
//
        Graph g2 = new Graph("G");
        g2.createRandomSimpleGraph(20, 30);
        System.out.println(g2.getMaxPathLength());
        System.out.println(g2.findLongestPaths(g2.first));
//
        Graph g3 = new Graph("G3");
        Vertex first3 = g3.createVertex("1");
        Vertex second3 = g3.createVertex("2");
        Vertex third3 = g3.createVertex("3");
        Vertex fourth3 = g3.createVertex("4");
        g3.createArc("arc32", third3, second3);
        g3.createArc("arc31", third3, first3);
        g3.createArc("arc21", second3, first3);
        g3.createArc("arc24", second3, fourth3);
        g3.createArc("arc13", first3, third3);
//
        System.out.println(g3.findLongestPaths(first3));
// throws exception
        Graph g4 = new Graph("G4");
        Vertex first4 = g4.createVertex("1");

        System.out.println(g4.findLongestPaths(first4));
    }

    /**
     * Koostada meetod, mis leiab etteantud sidusas lihtgraafis etteantud tipust v kõige kaugemal asuva tipu
     * Abi:
     * https://en.wikipedia.org/wiki/Longest_path_problem
     * http://stackoverflow.com/questions/27944340/longest-path-in-unweighted-undirected-graph
     * <p>
     * Kood 100% ise treitud.
     */
    public class Vertex {

        String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        //      ArrayList<Vertex> currentPathVertexes = new ArrayList<>();
        ArrayList<Arc> currentPathArcs = new ArrayList<>();
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * Is vertex already visited during the process of finding longest path
         * @param searchVertex Vertex
         * @return boolean
         */
        public boolean isVertexVisitedInArcPath(Vertex searchVertex) {
            for (Arc currentPathArc : currentPathArcs) {
                if (Objects.equals(currentPathArc.target.id, searchVertex.id)) {
                    return true;
                }
                if (Objects.equals(currentPathArc.from.id, searchVertex.id)) {
                    return true;
                }
            }
            return false;

        }

        /**
         * Get all arcs for Vertex
         * @return ArrayList
         */
        public ArrayList<Arc> getAllArcs() {
            ArrayList<Arc> arcs = new ArrayList<>();
            Arc currentArc = first;
            while (currentArc != null) {
                arcs.add(currentArc);
                currentArc = currentArc.next;
            }

            return arcs;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    public class Arc {

        private String id;
        Vertex target;
        private Arc next;

        private int info = 0;
        public Vertex from;
        // You can add more fields, if needed

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }


    public class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        public Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        public Graph(String s) {
            this(s, null);
        }

        /**
         * Find vertex count in graph
         * @return int
         */
        public int getVertexCount() {
            int count = 0;
            Vertex current = first;
            while (current != null) {
                count++;
                current = current.next;
            }
            return count;
        }

        public int getMaxPathLength() {
            return getVertexCount() - 1;
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" --->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            res.from = from;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Checks if given vertex is in current graph.
         * Check is done via vertex id
         * @param source Vertex
         * @return Vertex
         */
        public Vertex getVertexInGraph(Vertex source) {
            Vertex vertex = this.first;
            Vertex foundVertex = null;
            while (vertex != null) {
                if (Objects.equals(source.id, vertex.id)) {
                    foundVertex = vertex;
                }
                vertex = vertex.next;
            }
            return foundVertex;
        }

        /**
         * Finds longest paths for given vertex
         * @param source Vertex
         * @return ArrayList
         */
        public ArrayList<ArrayList<Arc>> findLongestPaths(Vertex source) {

            Vertex foundVertex = getVertexInGraph(source);
            if (foundVertex == null) {
                throw new RuntimeException("Vertex was not found in current graph");
            }
            if (foundVertex.getAllArcs().size() == 0) {
                throw new RuntimeException("Vertex is not connected, therefore the graph is invalid (in terms of the current task)");
            }
            LongestPathFinder longestPathFinder = new LongestPathFinder(this, foundVertex);
            return longestPathFinder.generatePaths().getLongestPaths();

//         String nl = System.getProperty ("line.separator");
//         StringBuffer sb = new StringBuffer (nl);

//         sb.append("Furthest vertexes(s) for vertex '");
//         sb.append(source.id);
//         sb.append("' is(are):");
//         sb.append(nl);
//         for (ArrayList<Vertex> path : vertexesInPaths) {
//            sb.append(path.get(path.size() - 1));
//            sb.append(" (Path: ");
//            for(int i = 0; i < path.size(); i++)
//            {
//               sb.append(path.get(i).id);
//               if (i != path.size() - 1) {
//                  sb.append("->");
//               }
//            }
//            sb.append(")");
//            sb.append(nl);
//         }


//         return sb;
        }
    }

    /**
     * Class to find longest path
     */
    public class LongestPathFinder {

        private Graph graph;
        private Vertex source;

        private int longestFoundPathLength = 0;


        private boolean isLongestPathFound = false;

        /**
         * Currently not used when queried
         */
//      private ArrayList<ArrayList<Vertex>> vertexesInPaths = new ArrayList<>();
        private ArrayList<ArrayList<Arc>> arcsInPaths = new ArrayList<>();

        /**
         * Constructor
         *
         * @param graph  Graph the graph where the given vertex is located
         * @param source Vertex this is the vertex which is the starting point
         */
        public LongestPathFinder(Graph graph, Vertex source) {
            this.graph = graph;
            this.source = source;
        }

        /**
         * Retruns longest paths from found paths
         * @return ArrayList
         */
        public ArrayList<ArrayList<Arc>> getLongestPaths() {
            ArrayList<ArrayList<Arc>> longestPaths = new ArrayList<>();
            if (arcsInPaths.size() == 0) {
                throw new RuntimeException("Paths are not generated");
            }

            int largestPath = arcsInPaths.get(0).size();

            for (ArrayList<Arc> path : arcsInPaths) {
                if (path.size() > largestPath) {
                    largestPath = path.size();
                }
            }

            // maybe we have more than 1 same length vertexesInPaths
            for (ArrayList<Arc> path : arcsInPaths) {
                if (path.size() == largestPath) {
                    longestPaths.add(path);
                }
            }
            return longestPaths;
        }

        /**
         * Starts generating paths
         *
         * @return this
         */
        public LongestPathFinder generatePaths() {
            resetChildrenVisitState(source);
            Vertex currentVertex = source;
//         currentVertex.currentPathVertexes.add(currentVertex); // must contain itself also
            parseChildVertexes(currentVertex);
            return this;
        }

        /**
         * Recursive method to parse vertexes
         *
         * @param currentVertex Vertex
         */
        private void parseChildVertexes(Vertex currentVertex) {
            if (!isLongestPathFound) {
                int currentBreaks = 0;

                int maxBreaks = currentVertex.getAllArcs().size();
                if (maxBreaks == 0) {
                    if (longestFoundPathLength <= currentVertex.currentPathArcs.size()) {
                        longestFoundPathLength = currentVertex.currentPathArcs.size();
                        arcsInPaths.add(new ArrayList<>(currentVertex.currentPathArcs));
                        if (graph.getMaxPathLength() == currentVertex.currentPathArcs.size()) {
                            // we have the max path now
                            isLongestPathFound = true;
                        }
                    }

//               vertexesInPaths.add(new ArrayList<>(currentVertex.currentPathVertexes));
                }
                for (Arc arc : currentVertex.getAllArcs()) {
                    if (!isLongestPathFound) {
                        if (currentVertex.isVertexVisitedInArcPath(arc.target)) {
                            currentBreaks++;

                            if (currentBreaks == maxBreaks) {
//                        ArrayList<Vertex> currentPathVertexes = new ArrayList<>(currentVertex.currentPathVertexes);
                                ArrayList<Arc> currentPathArcs = new ArrayList<>(currentVertex.currentPathArcs);
//                        vertexesInPaths.add(currentPathVertexes);
                                if (longestFoundPathLength <= currentPathArcs.size()) {
                                    longestFoundPathLength = currentPathArcs.size();
                                    arcsInPaths.add(currentPathArcs);
                                    if (graph.getMaxPathLength() == currentPathArcs.size()) {
                                        // we have the max path now
                                        isLongestPathFound = true;
                                    }
                                }

                                break;
                            }
//               // skip
                            continue;
                        }
                        Vertex children = arc.target;
//                  children.currentPathVertexes.clear();
                        children.currentPathArcs.clear();
//                  children.currentPathVertexes.addAll(new ArrayList<>(currentVertex.currentPathVertexes));
                        children.currentPathArcs.addAll(new ArrayList<>(currentVertex.currentPathArcs));
//                  children.currentPathVertexes.add(arc.target);
                        children.currentPathArcs.add(arc);

                        parseChildVertexes(children);
                    }
                }
            }
        }

        /**
         * This method resets all root vertex children visit states
         *
         * @param rootVertex Vertex
         */
        public void resetChildrenVisitState(Vertex rootVertex) {
//         rootVertex.currentPathVertexes = new ArrayList<>();
            rootVertex.currentPathArcs = new ArrayList<>();
            if (rootVertex.first != null && rootVertex.first.target != null) {
                Vertex childrenVertex = rootVertex.first.target;
//            childrenVertex.currentPathVertexes = new ArrayList<>();
                childrenVertex.currentPathArcs = new ArrayList<>();
                Vertex next = childrenVertex.next;
                while (next != null && next.currentPathArcs.size() != 0) {
                    resetChildrenVisitState(next);
                    next = next.next.first.target;
                }
            }

        }
    }
}

